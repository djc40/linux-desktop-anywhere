# Linux Desktop Anywhere
This is an install script to help get VMware Horizon set up to properly connect to Desktop Anywhere.

So far it's only been tested on Arch Linux, but hopefully I'll expand it in the future.
If on a different distro the main steps and packages will most likely stay the same but the file paths may differ.
You can also install the VMware Horizon client by downloading from their [website](https://my.vmware.com/en/web/vmware/downloads/info/slug/desktop_end_user_computing/vmware_horizon_clients/horizon_8) and running the bundle file yourself.

Let me know if there are any problems and I'll try to help troubleshoot to the best of my ability.

## Installation
- Clone the repo
- Change into the repo directory `cd linux-desktop-anywhere`
- Change permissions of the install script if necessary `chmod +x install.sh`
- Run the install script as a user `./install.sh`
## Troubleshooting
Check if your smart card is being detected? -- run `pcsc_scan` from a terminal window
