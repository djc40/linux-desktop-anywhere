#!/bin/bash

# References:
# https://public.cyber.mil/pki-pke/pkipke-document-library/
# https://wiki.archlinux.org/title/Common_Access_Card

# Check if running as root / sudo
if [ $EUID -eq 0 ]; then
    echo "$0 is running as root. Please run as a regular user."
    exit 2
fi

# Install required packages
echo "installing packages"
yay -Sy
yay -S ccid 
yay -S ca-certificates 
yay -S opensc 
yay -S vmware-horizon-client 
yay -S vmware-horizon-smartcard 
yay -S pcsc-tools

# Enable pcscd (Smart Card Daemon)
sudo systemctl enable pcscd.socket
sudo systemctl start pcscd.socket

# Get Certs and move them to the correct spot
cd $(mktemp -d)
curl https://dl.dod.cyber.mil/wp-content/uploads/pki-pke/zip/certificates_pkcs7_DoD.zip --output Certs.zip
unzip Certs.zip
rm Certs.zip
cd Cert*
sudo mv * /etc/ca-certificates/trust-source/anchors

# Check to make sure that the system is recognizing them
sudo update-ca-trust
sudo trust list | grep -i dod || \
    { echo "Looks like the certs aren't recognized, something may have gone wrong" ; exit 1; }

# Allow Vmware Horizon to interact with smart card
sudo mkdir -p /usr/lib/vmware/view/pkcs11
sudo ln -s /usr/lib/pkcs11/opensc-pkcs11.so /usr/lib/vmware/view/pkcs11/libopenpkcs11.so

# Tell the user its done
echo "Script is done, try launching VMware Horizon"
echo "Click \"New Server\""
echo "afrcdesktops.us.af.mil"
